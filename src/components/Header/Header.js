import React from "react"

function Header() {
  return (
    <header>
      <img
        src="https://drive.google.com/uc?id=1GWqw1nHbvwoaiG1lFMdYssA2pipEsFy-"
        alt="trollface"
      />
      <p>Meme Generator</p>
    </header>
  )
}
export default Header
