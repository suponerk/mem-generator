### MEME generator service written on React.js

This app generates meme from background image fetched from external API and your custom text.

Key features:

- Written using React JS
- Using external API to fetch images https://api.imgflip.com/get_memes/
- Using react-scripts to build and run app

How to start using App:

- First, you need Node.js and NPM installed on your host
- Run `npm install` to download all modules
- if you run this app locally, disable CORS in browser
- Run `npm run start` to start in dev mode
- OR run `npm run build` to build app
