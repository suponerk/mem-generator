import React, { Component } from "react"

class MemeGenerator extends Component {
  constructor() {
    super()
    this.state = {
      topText: "",
      bottomText: "",
      memImage: "https://i.imgflip.com/1ur9b0.jpg",
      memeBackgrounds: []
    }
    this.onChange = this.onChange.bind(this)
    this.onGenImg = this.onGenImg.bind(this)
  }

  componentDidMount() {
    fetch("https://api.imgflip.com/get_memes/")
      .then(response => response.json())
      .then(json => json.data.memes.map(item => item.url))
      .then(data => this.setState({ memeBackgrounds: data }))
  }

  onChange(event) {
    const { name, value } = event.target
    return this.setState({ [name]: value })
  }

  onGenImg() {
    const backgrounds = this.state.memeBackgrounds
    const max = backgrounds.length - 1
    const random = Math.floor(Math.random() * Math.floor(max))
    //console.log("random:", random)
    const randomImage = this.state.memeBackgrounds[random]
    return this.setState({ memImage: randomImage })
  }

  render() {
    //console.log("memeBackgrounds", this.state.memeBackgrounds)
    return (
      <div>
        <form className="meme-form">
          <input
            type="text"
            placeholder="Text on top"
            name="topText"
            value={this.state.topText}
            onChange={this.onChange}
          />
          <input
            type="text"
            placeholder="Text on bottom"
            name="bottomText"
            value={this.state.bottomText}
            onChange={this.onChange}
          />
          {/* tip: property type="button" prevents form to reload page */}
          <button type="button" onClick={this.onGenImg}>
            GenImg
          </button>
        </form>

        <div className="meme">
          <img align="center" src={this.state.memImage} alt="" />
          <h2 className="top">{this.state.topText}</h2>
          <h2 className="bottom">{this.state.bottomText}</h2>
        </div>
      </div>
    )
  }
}

export default MemeGenerator
