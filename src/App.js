import React from "react"
import Header from "./components/Header/Header"
import MemeGenerator from "./components/MemeGenerator/MemeGenerator"

class App extends React.Component {
  constructor() {
    super()
    this.state = {}
  }

  render() {
    return (
      <div>
        <Header />
        <MemeGenerator />
      </div>
    )
  }
}

export default App
